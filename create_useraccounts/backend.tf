terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.31.0"
    }
  }
  backend "s3" {
    # Replace this with your bucket name!
    bucket = "terraform-backend-s3-eu-central-1"
    key    = "terraform-state/iam_user/terraform.tfstate"
    region = "eu-central-1"
    profile = "tapasjana"
    #encrypt        = true
  }
}

provider "aws" {
  region  = var.region_name
  profile = "tapasjana"
}