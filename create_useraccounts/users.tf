# creating iam gorup from the group list available in variables.tf
resource "aws_iam_group" "iam_groups" {
  for_each = toset(var.group_names)

  name = each.value
}

# creating iam users from the user list available in variables.tf
resource "aws_iam_user" "iam_users" {
  for_each      = toset(var.user_names)
  name          = each.value
  path          = "/"
  force_destroy = true
}

# Attaching user: "User-Opsler-John" to Group: "group_devops"
resource "aws_iam_user_group_membership" "devops_user_membership" {
  user   = aws_iam_user.iam_users["User-devops-John"].name
  groups = [aws_iam_group.iam_groups["group_devops"].name]
}

# Attaching user: "User-qa-Martin" to Group: "group_tester"
resource "aws_iam_user_group_membership" "qa_user_membership" {
  user   = aws_iam_user.iam_users["User-qa-Martin"].name
  groups = [aws_iam_group.iam_groups["group_tester"].name]
}

# Creating a aws_access_key_id and aws_secret_access_key from keybase
# attaching to the users: User-devops-John
resource "aws_iam_access_key" "iam_users_key" {
  #for_each = toset(var.user_names)
  #user     = aws_iam_user.iam_users[each.value].name
  user    = aws_iam_user.iam_users["User-devops-John"].name
  pgp_key = file("${var.pgp_publickey}") //public key file
}


# adding a login profile to enable dashboard login for all users
resource "aws_iam_user_login_profile" "user_profile" {
  for_each                = toset(var.user_names)
  user                    = aws_iam_user.iam_users[each.value].name
  pgp_key                 = var.pgp_keybase_name
  password_reset_required = true
}

