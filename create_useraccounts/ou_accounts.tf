/*
data "aws_organizations_organization" "org" {}

resource "aws_organizations_organizational_unit" "ou_devops" {
  name = "OU-DEVOPS"
  parent_id = data.aws_organizations_organization.org.roots[0].id
}


resource "aws_organizations_account" "ac_developer" {
  name      = "Account-Developer"
  email     = "account1@example.com"
  role_name = "OrganizationAccountAccessRole"  # You can customize the role name
  parent_id = aws_organizations_organizational_unit.ou_devops.id
}

resource "aws_organizations_account" "ac_opsler" {
  name      = "Account-Opsler"
  email     = "account2@example.com"
  role_name = "OrganizationAccountAccessRole"  # You can customize the role name
  parent_id = aws_organizations_organizational_unit.ou_devops.id
}

*/
