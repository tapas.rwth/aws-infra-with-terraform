output "login_password" {
    description = "login password for devops user"
    value = aws_iam_user_login_profile.user_profile[tolist(var.user_names)[0]].encrypted_password
}

output "aws_access_key_id" {
    description = "aws_access_key_id for devops user"
    value = aws_iam_access_key.iam_users_key.id
}

output "aws_secret_access_key" {
    description = "aws_secret_access_key for devops user"
    value = aws_iam_access_key.iam_users_key.encrypted_secret
}