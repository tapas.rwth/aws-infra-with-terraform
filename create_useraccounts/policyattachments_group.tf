
resource "aws_iam_group_policy_attachment" "beanstalk_admin_policy" {
  group      = aws_iam_group.iam_groups["group_devops"].name
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess-AWSElasticBeanstalk"
}

resource "aws_iam_group_policy_attachment" "beanstalk_readonly_policy" {
  group      = aws_iam_group.iam_groups["group_tester"].name
  policy_arn = "arn:aws:iam::aws:policy/AWSElasticBeanstalkReadOnly"
}


/*
resource "aws_iam_group_policy_attachment" "s3_full_access_policy" {
  group = aws_iam_group.iam_groups["group_devops"].name
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}
resource "aws_iam_group_policy_attachment" "ec2_full_access_policy" {
  group = aws_iam_group.iam_groups["group_devops"].name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2FullAccess"
}

resource "aws_iam_group_policy_attachment" "beanstalk_admin_policy" {
  for_each   = aws_iam_group.iam_groups
  group      = name
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess-AWSElasticBeanstalk"
}

*/