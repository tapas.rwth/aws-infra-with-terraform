variable "group_names" {
  #type    = list(string)
  type    = set(string)
  default = ["group_devops", "group_tester"] # Add your group names here
}

variable "user_names" {
  #type    = list(string)
  type    = set(string)
  default = ["User-devops-John", "User-qa-Martin"] # Add your group names here: "User-qa-Martin"
}

variable "region_name" {
  type    = string
  default = "eu-central-1"
}

variable "pgp_keybase_name" {
  type    = string
  default = "keybase:<yourkeybase name>" 
}

variable "pgp_publickey" {
  type    = string
  default = "./my_public.key"
}