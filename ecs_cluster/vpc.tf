module "my_vpc" {
  source                           = "../modules/vpc"
  region_name                      = var.region_name
  vpc_name                         = var.vpc_name
  vpc_cidr                         = var.vpc_cidr
  set_subnet_count                 = local.set_subnet_count
  enable_dns_hostnames             = var.enable_dns_hostnames
  enable_dns_support               = var.enable_dns_support
  assign_generated_ipv6_cidr_block = var.assign_generated_ipv6_cidr_block
  igw_name                         = var.igw_name
  nat_gw_enabled                   = var.nat_gw_enabled
}

