variable "region_name" {
    type = string
    default = "eu-central-1"
}

variable "profile_name" {
    type = string
    default = "tapasjana"
}

variable "vpc_name" {
    type = string
    default = "ECS-Demo-VPC"
}

variable "vpc_cidr" {
    type = string
    default = "10.0.0.0/16"
}

variable "enable_dns_hostnames" {
    type = string
    default = true
}

variable "enable_dns_support" {
    type = bool
    default = true
}

variable "assign_generated_ipv6_cidr_block" {
    type = bool
    default = false
}

variable "igw_name" {
    type = string
    default = "ECS-Demo-VPC-IGW"
}


data "aws_availability_zones" "available" {
  state = "available"
}

variable "eks_cluster_name" {
    type = string
    default = "My-ECS-Cluster"
}

variable "eks_node_group_name" {
    type = string
    default = "My-EcS-node-group"
}

variable "project_name" {
    type = string
    default = "My-ECS-Project"
}

variable "nat_gw_enabled" {
  type = bool
  default = false
}

variable "ecr_repository_name" {
   type    = string
   default = "my-ecr-repository"
}

variable "ecs_cluster_name" {
   type    = string
   default = "my-ecs-cluster"
}

variable "ecs_task_definition"{
  type = string
  default = "ecs-task-def"
}

variable "app_name" {
  type = string
  default = "nginx"
}

variable "app_environment" {
    type = string
    default = "dev" 
}

locals {
    #set_subnet_count = length(data.aws_availability_zones.available.names)
    set_subnet_count = 2
    all_subnet_ids = concat(module.my_vpc.private_subnets_ids, module.my_vpc.public_subnets_ids)
}

