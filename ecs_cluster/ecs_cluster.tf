# ECR
/*
resource "aws_ecr_repository" "aws-ecr" {
  name = "${var.ecr_repository_name}"
  image_tag_mutability = "MUTABLE"
  force_delete = "true"
}
*/

# iam.tf | IAM Role Policies

resource "aws_iam_role" "ecsTaskExecutionRole" {
  name= "ecs-execution-task-role"
  assume_role_policy = data.aws_iam_policy_document.assume_role_policy.json
}

data "aws_iam_policy_document" "assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

resource "aws_iam_role_policy_attachment" "ecsTaskExecutionRole_policy" {
  role       = aws_iam_role.ecsTaskExecutionRole.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}


resource "aws_ecs_cluster" "aws-ecs-cluster" {
  name = "${var.ecs_cluster_name}"
}

#resource "aws_cloudwatch_log_group" "log-group" {
  #name = "${var.app_name}-${var.app_environment}-logs"
#  name = "ecs/nginx"
#}

#data "template_file" "env_vars" {
#  template = file("env_vars.json")
#}

resource "aws_ecs_task_definition" "aws-ecs-task" {
  family = "${var.ecs_task_definition}"
  container_definitions = "${file("task-definition.json")}"
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  memory                   = "3072"
  cpu                      = "1024"
  #execution_role_arn       = aws_iam_role.ecsTaskExecutionRole.arn
  #task_role_arn            = aws_iam_role.ecsTaskExecutionRole.arn
  execution_role_arn      = "arn:aws:iam::164913430053:role/ecsTaskExecutionRole"
  task_role_arn           = "arn:aws:iam::164913430053:role/ecsTaskExecutionRole"
  runtime_platform {
        cpu_architecture = "X86_64"
        operating_system_family = "LINUX"
    }

}

data "aws_ecs_task_definition" "main" {
  task_definition = aws_ecs_task_definition.aws-ecs-task.family
}

resource "aws_ecs_service" "aws-ecs-service" {
  name                 = "my-ecs-service"
  cluster              = aws_ecs_cluster.aws-ecs-cluster.id
  task_definition      = "${aws_ecs_task_definition.aws-ecs-task.family}:${max(aws_ecs_task_definition.aws-ecs-task.revision, data.aws_ecs_task_definition.main.revision)}"
  launch_type          = "FARGATE"
  scheduling_strategy  = "REPLICA"
  desired_count        = 1
  force_new_deployment = true
  health_check_grace_period_seconds = 5
  network_configuration {
    subnets          = module.my_vpc.public_subnets_ids
    assign_public_ip = true
    security_groups = [
      aws_security_group.service_security_group.id,
      aws_security_group.load_balancer_security_group.id
    ]
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.target_group.arn
    container_name   = "nginx"
    container_port   = 80
  }

  depends_on = [aws_lb_listener.listener]
}