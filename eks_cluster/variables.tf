variable "region_name" {
    type = string
    default = "eu-central-1"
}

variable "profile_name" {
    type = string
    default = "tapasjana"
}

variable "vpc_name" {
    type = string
    default = "EKS-Demo-VPC"
}

variable "vpc_cidr" {
    type = string
    default = "10.0.0.0/16"
}

variable "enable_dns_hostnames" {
    type = string
    default = true
}

variable "enable_dns_support" {
    type = bool
    default = true
}

variable "assign_generated_ipv6_cidr_block" {
    type = bool
    default = false
}

variable "igw_name" {
    type = string
    default = "EKS-Demo-VPC-IGW"
}


data "aws_availability_zones" "available" {
  state = "available"
}

variable "eks_cluster_name" {
    type = string
    default = "My-EKS-Cluster"
}

variable "eks_node_group_name" {
    type = string
    default = "My-EKS-node-group"
}

variable "project_name" {
    type = string
    default = "My-EKS-Project"
}

variable "nat_gw_enabled" {
  type = bool
  default = false
}


locals {
    set_subnet_count = length(data.aws_availability_zones.available.names)
    all_subnet_ids = concat(module.my_vpc.private_subnets_ids, module.my_vpc.public_subnets_ids)
}

# -------------- add on---------
variable "addons" {
  type = list(object({
    name    = string
    version = string
  }))

  default = [
    {
      name    = "kube-proxy"
      version = "v1.28.1-eksbuild.1"
    },
    {
      name    = "vpc-cni"
      version = "v1.14.1-eksbuild.1"
    },
    {
      name    = "coredns"
      version = "v1.10.1-eksbuild.2"
    },
    {
      name    = "eks-pod-identity-agent"
      version = "v1.0.0-eksbuild.1"
    }
  ]
}