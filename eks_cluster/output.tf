output "eks_endpoint" {
  value = aws_eks_cluster.eks_cluster.endpoint
}

output "eks_cluster_name" {
  value = aws_eks_cluster.eks_cluster.name
}

#output "kubeconfig-certificate-authority-data" {
#  value = aws_eks_cluster.eks_cluster.certificate_authority[0].data
#}



output "all-subnet-ids" {
    value = local.all_subnet_ids
}