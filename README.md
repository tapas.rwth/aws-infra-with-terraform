## Create AWS Infrastructure using Terraform

#### AWS Infrastructure High Level design diagram
![small](/project-images/architecture.png)

Docker application will run in EC2 insatnces launched with a autoscaling group in 2 Public Subnets behind a Loadbalancer. According to load the autoscaling group can add more EC2 instances when it is required. EC2 and Loadbalancer metrics are monitoried in Cloudwatch. High Level design is presented in a separate High Level Design Document.

Components:
<br/>VPC (from /modules/vpc):
<br/>Beanstalk (from /modules/beanstalk):
<br/>Cloudtrail:
<br/>Cloudwatch log gorup:
<br/>S3 Bucket for Elasticbeanstalk app versioning:
<br/>S3 for storing cloud traillogs:
<br/>S3 Bucket for terraform backend: which is created before the running "create_vpc_beanstalk" code

code structure
````
.
├── README.md
├── create_useraccounts    # it creats 2 users and attach iam polidy and login profile
│   ├── backend.tf
│   ├── ou_accounts.tf
│   ├── output.tf
│   ├── policyattachments_group.tf
│   ├── users.tf
│   └── variables.tf
|
├── create_vpc_beanstalk   # created from modules: vpc and beanstalk
│   ├── backend.tf
│   ├── cloudtrail.tf
│   ├── dev.tfvars
│   ├── main.tf
│   ├── output.tf
│   ├── prod.tfvars
│   ├── stage.tfvars
│   ├── variables.tf
│   └── worknote.txt
├── eks_cluster  # to create a simple eks cluster
│   ├── cluster-setup-manual # to prepare cluster: install argocd, install nginx-ingress controller
|   ├── backend.tf
│   ├── eks_cluster.tf
│   ├── eks_node_group.tf
│   ├── output.tf
│   ├── variables.tf
|   └── vpc.tf
├── modules
│   ├── beanstalk            # module to create beanstalk
│   │   ├── README.md
│   │   ├── beanstalk.tf
│   │   ├── output.tf
│   │   └── variables.tf
│   └── vpc                 # module to create vpc: vpc+igw+public subnet+private subnet+routing table
│       ├── main.tf
│       ├── output.tf
│       └── variables.tf
├── project-images
│   ├── architecture.png
│   ├── cicd.png
│   ├── iac.png
│   └── iac_pipeline_view.png
└── test_custom_runner          # To test custom gitlab runner Docker image
    ├── Dockerfile
    ├── backend.tf
    ├── build.sh
    ├── hello.txt
    ├── main.tf
    └── script.sh
````

#### Gitlab CI/CD Pipeline with Terraform
![iac-pipeline](/project-images/iac.png)

To run the terraform code and create infrastructure in aws following terraform commands are used:

Requirements: Assuming that AWS account is present with a user having programatic access
And following variables are passed during code execution as Environment variables. In the Gitlab CI they are stores as Environment variables
<br>_AWS_ACCESS_KEY_ID_
<br>_AWS_SECRET_ACCESS_KEY_
<br>_AWS_DEFAULT_REGION_

Runner image: hashicorp/terraform:light
</br>The gitlab cicd code defined in <b>.gitlab-ci.yml</b>, creates following pipeline in Gitlab

Summary of the pipeline:
````
# Available environment names: [dev, stage, prod]

# To separate env and save states in same backend, Workspace is created based on environment name: workspace_name = $CI_ENVIRONMENT_NAME

stage: validate
- cd /create_vpc_beanstalk
- terraform validate #terraform validates the code

stage: apply
- cd /create_vpc_beanstalk
- terraform workspace select $CI_ENVIRONMENT_NAME || terraform workspace new $CI_ENVIRONMENT_NAME
- terraform workspace list
- terraform plan -input=false -var-file=$(terraform workspace show).tfvars
- terraform apply -auto-approve -input=false -var-file=$(terraform workspace show).tfvars

stage: destroy
- cd /create_vpc_beanstalk
- terraform workspace select $CI_ENVIRONMENT_NAME || terraform workspace new $CI_ENVIRONMENT_NAME
- terraform destroy -auto-approve -input=false -var-file=$(terraform workspace show).tfvars
````
#### Pipeline view in gitlab

![pipeline view in gitlab](/project-images/iac_pipeline_view.png)

To run the terraform code from local pc and create infrastructure in aws.
Assuming that aws_cli is installed and _aws_access_key_id_ and _aws_secret_access_key_ is stored in aws-cli config

#### IAM Users: 
##### user1: User-devops-John and user2: User-qa-Martin
##### group1: group_devops and group2: group_tester

User-devops-John is part of a group: group_devops and User-qa-Martin is part of group: group_tester
User-devops-John has: Elastic Beanstalk admin policy
User-qa-Martin has: Elastic Beanstalk readonly policy

AWS access_key_id and secret_access_keys are created by pgp key managed by keybase profile.
````
# Creating a aws_access_key_id and aws_secret_access_key from keybase
resource "aws_iam_access_key" "iam_users_key" {
  user    = aws_iam_user.iam_users["User-devops-John"].name
  pgp_key = file("./my_pgp_public.key") //public key file
}


# adding a login profile to enable dashboard login for all users
resource "aws_iam_user_login_profile" "user_profile" {
  for_each                = toset(var.user_names)
  user                    = aws_iam_user.iam_users[each.value].name
  pgp_key                 = keybase:<yourkeybase name>  //keybase name
  password_reset_required = true
}


# Create a pgp key pair
$gpg --full-generate-key

# Export the public and private keys
$gpg -export -a <keyid-displayed-in-console> > my_public.key
$gpg --export-secret-key -a <keyid-displayed-in-console> > my_private.key

# Import your private key generated by gpg command to Keybase server:
$keybase pgp import -i <private.key>

To decrypt password/secret generated from Terraform output:
$terraform output -raw login_password | base64 --decode | keybase pgp decrypt
$terraform output -raw aws_secret_access_key | base64 --decode | keybase pgp decrypt  
````
#### To create IAM Users, groups and policy: 
````
cd ./create_useraccounts
$terraform init
$terraform plan
$terraform apply
````

#### To create Elastic Beanstalk
````
# available env: [dev, stage, prod]

# go to create_vpc_beanstalk folder
$ cd ./create_vpc_beanstalk

# initialize terraform
$terraform init

# create namespaces
$terraform workspace new dev
$terraform workspace new stage
$terraform workspace new prod

# list available workspaces
$terraform workspace list

# select namespace
$terraform workspace select <namespace_name>
example: $terraform workspace select dev

# Create Infrastructure
$terraform plan -var-file=<namespace_name>.tfvars
$terrafrom apply -var-file=<namespace_name>.tfvars

# Destroy Infrastructure
$terraform destroy -var-file=<namespace_name>.tfvars
````