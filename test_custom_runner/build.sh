#!/bin/bash
image_name=registry.gitlab.com/tapas.rwth/aws-infra-with-terraform/gitlab_runner
tag=1.0
docker build -t $image_name:$tag .
docker push $image_name:$tag