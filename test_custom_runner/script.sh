#!/bin/bash
aws s3 ls
# remove bucket-policy
account_id=$(aws sts get-caller-identity --query "Account" --output text)
region=${AWS_DEFAULT_REGION}
echo "account id: ${account_id}"
#echo "region: ${region}"
#aws s3api delete-bucket-policy --bucket elasticbeanstalk-$region-$account_id

# delete objects recursively
#aws s3 rm s3://elasticbeanstalk-$region-$account_id --recursive

# delete bucket
#aws s3 rb s3://elasticbeanstalk-$region-$account_id