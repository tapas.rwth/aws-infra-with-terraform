resource "aws_s3_bucket" "my_s3" {
    bucket = "my-test-bucket-tapasjana"
}

resource "aws_s3_object" "object" {
  bucket = aws_s3_bucket.my_s3.id
  key    = "hello.txt"
  source = "./hello.txt"
}


# resource "terraform_data" "example1" {
#   provisioner "local-exec" {
#      command = "./script.sh"
#      interpreter = ["bash", "-c"]
# }

#   provisioner "local-exec" {
#    when = destroy
#    command = "./script.sh"
#    interpreter = ["bash", "-c"]
#  }

# }
