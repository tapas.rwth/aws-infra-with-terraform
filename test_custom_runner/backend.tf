terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.31.0"
    }
  }
  backend "s3" {
    # Replace this with your backend bucket name!
    bucket  = "terraform-backend-s3-eu-central-1"
    key     = "terraform-state/s3/terraform.tfstate"
    region  = "eu-central-1"
    #profile = "tapasjana"
  }
}

provider "aws" {
  region  = "eu-central-1"
  #profile = "tapasjana"
  default_tags {
    tags = {
      created_by = "Tapas Jana"
    }
  }
}
