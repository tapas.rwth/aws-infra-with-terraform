output "vpc_id" {
  description = "ID of the VPC"
  value       = module.my_vpc.vpc_id
}
/*
output "az_count" {
    value = "${length(data.aws_availability_zones.available.names)}"
    description = "Available AZ Count in the Region"
}
*/
output "public_subnets" {
  description = "Public subnets of the VPC"
  value       = module.my_vpc.public_subnets_ids
}

output "private_subnets" {
  description = "Private subnets of the VPC"
  value       = module.my_vpc.private_subnets_ids
}


output "eb_application_name" {
  value       = module.my_elasticbeanstalk_app.eb_application_name
  description = "The Elastic Beanstalk Application for this environment"
}

output "eb_environment_name" {
  value       = module.my_elasticbeanstalk_app.eb_environment_name
  description = "Elastic Beanstalk ENV Name"
}

output "eb_environment_cname" {
  value       = module.my_elasticbeanstalk_app.eb_environment_cname
  description = "Elastic Beanstalk ENV DNS Name"
}

output "load_balancers_dns" {
  value       = module.my_elasticbeanstalk_app.load_balancers_dns
  description = "ELB DNS Name"
}
