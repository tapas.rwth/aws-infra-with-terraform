module "my_vpc" {
  source                           = "../modules/vpc"
  region_name                      = var.region_name
  vpc_name                         = local.my_vpc_name
  vpc_cidr                         = var.vpc_cidr
  set_subnet_count                 = var.set_subnet_count
  enable_dns_hostnames             = var.enable_dns_hostnames
  enable_dns_support               = var.enable_dns_support
  assign_generated_ipv6_cidr_block = var.assign_generated_ipv6_cidr_block
  igw_name                         = local.my_igw_name

}

module "my_elasticbeanstalk_app" {
  source               = "../modules/beanstalk"
  eb_app_name          = local.my_eb_app_name
  eb_env_name          = local.my_eb_env_name
  solution_stack_name  = var.solution_stack_name
  application_tier     = var.application_tier
  aws_vpc_id           = module.my_vpc.vpc_id
  selected_ec2_subnets = module.my_vpc.public_subnets_ids
  selected_elb_subnets = module.my_vpc.public_subnets_ids
  asg_min_size         = var.asg_min_size
  asg_max_size         = var.asg_max_size
  region_name          = var.region_name
  vpc_name             = local.my_vpc_name
  vpc_cidr             = var.vpc_cidr
  ec2_keypair          = var.ec2_keypair
  igw_name             = local.my_igw_name
  instance_type        = var.instance_type
  bucket_force_destroy = var.bucket_force_destroy
  elb_healthcheck_path = var.elb_healthcheck_path
  eb_s3_bucket_name    = local.my_eb_s3_bucket_name

}

/*
resource "terraform_data" "destroy_bucket_eb" {
  provisioner "local-exec" {
    when        = destroy
    command     = "./remove_beanstalkbucket_script.sh"
    interpreter = ["bash", "-c"]
  }
}
*/