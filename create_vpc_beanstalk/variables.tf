variable "region_name" {}

variable "profile_name" {}

variable "vpc_name" {}

variable "vpc_cidr" {}

variable "enable_dns_hostnames" {}

variable "enable_dns_support" {}

variable "assign_generated_ipv6_cidr_block" {}

variable "igw_name" {}

variable "eb_app_name" {}
variable "eb_env_name" {}
variable "asg_min_size" {}
variable "asg_max_size" {}
variable "ec2_keypair" {}
variable "instance_type" {}
variable "bucket_force_destroy" {}
variable "elb_healthcheck_path" {}
variable "solution_stack_name" {}
variable "application_tier" {}
variable "eb_s3_bucket_name" {
  default = "elasticbeanstalk-s3"
}
#variable "selected_ec2_subnets" {}

#variable "selected_elb_subnets" {}
#variable "aws_vpc_id" {}

# data "aws_vpc" "selected" {
#   tags = {
#     Name = "${var.vpc_name}"
#   }
# }

variable "project_name" {
  type = string
  #default = "My-project"
}

variable "env_name" {
  type = string
  #default = "DEV"
}

locals {
  #selected_public_subnets  = flatten(module.my_vpc.public_subnets_ids)
  #selected_private_subnets = flatten(module.my_vpc.private_subnets_ids)
  my_eb_app_name       = "${var.eb_app_name}-${var.env_name}"
  my_eb_env_name       = "${var.eb_env_name}-${var.env_name}"
  my_eb_s3_bucket_name = "${var.eb_s3_bucket_name}-${var.env_name}-${var.region_name}"
  my_vpc_name          = "${var.vpc_name}-${var.env_name}"
  my_igw_name          = "${var.igw_name}-${var.env_name}"
  cloudtrail_name      = "Cloudtrail-EB-Project-${var.env_name}-${var.region_name}"
}

data "aws_availability_zones" "available" {
  state = "available"
}

variable "set_subnet_count" {}