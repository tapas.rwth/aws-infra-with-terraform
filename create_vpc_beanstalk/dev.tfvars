project_name                     = "spring-boot-react-rest"
region_name                      = "eu-central-1"
profile_name                     = "tapasjana"
vpc_name                         = "My-VPC"
vpc_cidr                         = "10.0.0.0/16"
set_subnet_count                 = 2
enable_dns_hostnames             = true
enable_dns_support               = true
assign_generated_ipv6_cidr_block = false
igw_name                         = "My-IGW"
env_name                         = "dev"

eb_app_name          = "My-eb-docker-app"
eb_env_name          = "My-eb-docker-app-env"
solution_stack_name  = "64bit Amazon Linux 2023 v4.1.2 running Docker"
application_tier     = "WebServer"
asg_min_size         = "1"
asg_max_size         = "2"
ec2_keypair          = "tapasjana-keypair"
instance_type        = "t2.micro"
bucket_force_destroy = true
elb_healthcheck_path = "/login"

