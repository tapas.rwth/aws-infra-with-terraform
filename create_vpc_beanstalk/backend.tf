terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.31.0"
    }
  }
  backend "s3" {
    # Replace this with your backend bucket name!
    bucket = "terraform-backend-s3-eu-central-1"
    key    = "terraform-state/beanstalk/terraform.tfstate"
    region = "eu-central-1"
    #encrypt        = true
  }
}

provider "aws" {
  region = var.region_name
  #profile = var.profile_name
  default_tags {
    tags = {
      project    = var.project_name
      created_by = "Tapas Jana"
      ENV        = var.env_name
    }
  }
}

