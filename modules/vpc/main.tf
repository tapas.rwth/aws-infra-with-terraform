#-------------VPC-------------------------
resource "aws_vpc" "vpc"{
    cidr_block   = var.vpc_cidr
    enable_dns_hostnames = var.enable_dns_hostnames
    enable_dns_support   = var.enable_dns_support
    assign_generated_ipv6_cidr_block = var.assign_generated_ipv6_cidr_block
    tags = {
        Name = var.vpc_name
    }
}

# ------------Internet Gateway -----------------
resource "aws_internet_gateway" "igw"{
    vpc_id = aws_vpc.vpc.id
    tags = {
        Name = var.igw_name
    }
}

# ---------------------- PUBLIC SUBNET AND ROUTE TABLE ASSOCIATION --------------------
data "aws_availability_zones" "available" {
  state = "available"
}

resource "aws_subnet" "public_subnets" {
    vpc_id = aws_vpc.vpc.id
    count = "${var.set_subnet_count}"
    availability_zone = "${data.aws_availability_zones.available.names[count.index]}"
    cidr_block = "${cidrsubnet(var.vpc_cidr,8, count.index + local.az_count)}"
    map_public_ip_on_launch = true
    tags = {
        Name = "Public-Subnet-${count.index + 1}"
        Mode = "Public"
        VPC_NAME =  "${var.vpc_name}"
    }

}

resource "aws_route_table" "public_rt" {
    vpc_id = aws_vpc.vpc.id

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.igw.id
    }
    tags = {
        Name = "Public-RT"
    }

}

resource "aws_route_table_association" "public_rt_association" {
    #count = "${length(data.aws_availability_zones.available.names)}"
    count = "${var.set_subnet_count}"
    subnet_id      = element(aws_subnet.public_subnets[*].id, count.index)
    route_table_id = aws_route_table.public_rt.id

}

# -----------------------PRIVATE SUBNET AND ROUTE TABLE ASSOCIATION----------------------

resource "aws_subnet" "private_subnets" {
    vpc_id = aws_vpc.vpc.id
    #count = "${length(data.aws_availability_zones.available.names)}"
    count = "${var.set_subnet_count}"
    #cidr_block = "10.0.${count.index + 10}.0/24"
    cidr_block = "${cidrsubnet(var.vpc_cidr,8,count.index)}"
    availability_zone = "${data.aws_availability_zones.available.names[count.index]}"
    map_public_ip_on_launch = true
    tags = {
        Name = "Private-Subnet-${count.index + 1}"
        Mode = "Private"
        VPC_NAME =  "${var.vpc_name}"
    }

}

resource "aws_route_table" "private_rt" {
    vpc_id = aws_vpc.vpc.id
    count = var.set_subnet_count
    #route {
      #cidr_block              = "0.0.0.0/0"
      #nat_gateway_id          = ""
      #nat_gateway_id          = aws_nat_gateway.nat_gw[count.index].id
    #}
    /*
    route = [
    {
      cidr_block                 = "0.0.0.0/0"
      #nat_gateway_id             = element(aws_nat_gateway.nat_gw.*.id, count.index)
      /*
      carrier_gateway_id         = ""
      destination_prefix_list_id = ""
      egress_only_gateway_id     = ""
      gateway_id                 = ""
      instance_id                = ""
      ipv6_cidr_block            = ""
      local_gateway_id           = ""
      network_interface_id       = ""
      transit_gateway_id         = ""
      vpc_endpoint_id            = ""
      vpc_peering_connection_id  = ""
      
    }
*/
    tags = {
        Name = "Private-RT-${var.vpc_name}-${count.index+1}"
    }

}

resource "aws_route" "private_nat_gateway" {
    count = var.nat_gw_enabled? var.set_subnet_count : 0
    route_table_id         = element(aws_route_table.private_rt.*.id, count.index)
    destination_cidr_block = "0.0.0.0/0"
    nat_gateway_id         = aws_nat_gateway.nat_gw[count.index].id
}

/*
resource "aws_route_table_association" "private_rt_association" {
    #count = "${length(data.aws_availability_zones.available.names)}"
    count          = "${var.set_subnet_count}"
    subnet_id      = element(aws_subnet.private_subnets[*].id, count.index)
    route_table_id = aws_route_table.private_rt.id

}
*/
resource "aws_route_table_association" "private_rt_association" {
  count          = "${var.set_subnet_count}"
  subnet_id      = element(aws_subnet.private_subnets.*.id, count.index)
  route_table_id = element(aws_route_table.private_rt.*.id, count.index)
}

# ------------------ NAT Gateway ----------------------------
# Elastic IP for NAT
resource "aws_eip" "nat_eip" {
    count      = var.nat_gw_enabled? var.set_subnet_count : 0
    domain     = "vpc"
    depends_on = [aws_internet_gateway.igw]
}
# NAT Gateway
resource "aws_nat_gateway" "nat_gw" {
    count         = var.nat_gw_enabled? var.set_subnet_count : 0
    #allocation_id = "${aws_eip.nat_eip.id}"
    allocation_id = "${element(aws_eip.nat_eip.*.id, count.index)}"
    subnet_id     = "${element(aws_subnet.public_subnets.*.id, count.index)}"
    depends_on    = [aws_internet_gateway.igw]
    tags = {
        Name        = "My-NAT-${var.vpc_name}-${count.index+1}"
    }
}
# --------------------------------------------------------------