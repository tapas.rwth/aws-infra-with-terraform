# --------Create Elastic Beanstalk ----------
Create eb application
Create eb environemnt within the eb application



#### How to use the module
````
module "my_vpc" {
  source                           = "<module path relative to the code execution path>"
  region_name                      = var.region_name
  vpc_name                         = local.my_vpc_name
  vpc_cidr                         = var.vpc_cidr
  set_subnet_count                 = var.set_subnet_count
  enable_dns_hostnames             = var.enable_dns_hostnames
  enable_dns_support               = var.enable_dns_support
  assign_generated_ipv6_cidr_block = var.assign_generated_ipv6_cidr_block
  igw_name                         = local.my_igw_name

}

example arguments:
region_name                      = "eu-central-1"
vpc_name                         = "My-VPC"
vpc_cidr                         = "10.0.0.0/16"
set_subnet_count                 = 2
enable_dns_hostnames             = true
enable_dns_support               = true
assign_generated_ipv6_cidr_block = false
igw_name                         = "My-IGW"
````

---To run the terraform code and create infra----
````
available env: [dev, stage, prod]

create namespaces
$terraform workspace new dev
$terraform workspace new stage

select namespaces and run code
$terraform workspace select <namespace_name>
$terraform plan
$terrafrom apply/destroy

````
# test auto-scaling by giving load with apache-bench tool
$ab -n 10000 -c 1000 <url>


