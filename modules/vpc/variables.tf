

variable "region_name" {
  type    = string
  #default = "eu-central-1"
}

/*
variable "profile_name" {
  type    = string
  #default = "tapasjana"
}
*/


variable "vpc_name" {
  type    = string
  #default = "My-VPC"
}

variable "vpc_cidr" {
  type    = string
  #default = "10.0.0.0/16"
}

variable "set_subnet_count" {
  type    = string
  default = "2"
}

variable "enable_dns_hostnames" {
  type = bool
  default= true
}

variable "enable_dns_support" {
  type = bool
  default= true
}

variable "assign_generated_ipv6_cidr_block" {
  type = bool
  default= false
}

variable "igw_name" {
  type    = string
  #default = "My-IGw"
}

variable "nat_gw_enabled" {
  type = bool
  default = false
  description = "Enable or disbale NAT Gateway for Private-subnet"
}

locals {
    az_count = length(data.aws_availability_zones.available.names)
}
