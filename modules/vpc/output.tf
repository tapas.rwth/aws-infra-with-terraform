output "vpc_id" {
  description = "ID of the VPC"
  value       = aws_vpc.vpc.id
}


output "public_subnets_ids" {
  description = "Public subnets of the VPC"
  value       = aws_subnet.public_subnets[*].id
}

output "private_subnets_ids" {
  description = "Public subnets of the VPC"
  value       = aws_subnet.private_subnets[*].id
}
