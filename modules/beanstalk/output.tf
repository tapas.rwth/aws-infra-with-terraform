
/*
output "vpc_id" {
  description = "ID of project VPC"
  value       = data.aws_vpc.selected.id
}
output "public_subnets" {
  description = "Public subnets of the VPC"
  value       = data.aws_subnets.public
}

output "private_subnets" {
  description = "Private subnets of the VPC"
  value       = data.aws_subnets.private
}

output "selected_public_subnets" {
  #value = element(data.aws_subnets.public.ids,1)
  value = "${slice(data.aws_subnets.public.ids,0,2)}"

}

*/



output "eb_application_name" {
  value       = aws_elastic_beanstalk_application.eb_app.name
  description = "The Elastic Beanstalk Application for this environment"
}

output "eb_environment_name" {
  value       = aws_elastic_beanstalk_environment.eb_env.name
  description = "Elastic Beanstalk ENV Name"
}

output "eb_environment_cname" {
  value       = aws_elastic_beanstalk_environment.eb_env.cname
  description = "Elastic Beanstalk ENV DNS Name"
}

output "load_balancers_dns" {
  value       = data.aws_lb.eb_lb.dns_name
  description = "ELB DNS Name"
}

/*
output "load_balancers_arn" {
  value       = aws_elastic_beanstalk_environment.eb_env.load_balancers[0]
  description = "Elastic Load Balancers in use by this environment"
}
*/
