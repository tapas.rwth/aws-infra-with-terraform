data "aws_availability_zones" "available" {
  state = "available"
}

variable "eb_app_name" {
  type = string
  #default = "My-eb-docker-app"
}

variable "eb_env_name" {
  type = string
  #default = "My-eb-docker-app-env"
}

variable "solution_stack_name" {
  type = string
}
variable "application_tier" {
  type = string
}

variable "asg_min_size" {
  type = string
}

variable "asg_max_size" {
  type = string
}

variable "region_name" {
  type    = string
  #default = "eu-central-1"
}

variable "aws_vpc_id" {
  type = string
}

variable "vpc_name" {
  type    = string
  #default = "My-VPC"
}

variable "vpc_cidr" {
  type    = string
  #default = "10.0.0.0/16"
}

variable "ec2_keypair" {
  type = string
}

variable "igw_name" {
  type    = string
}

variable "instance_type" {
  type = string
}


variable "bucket_force_destroy" {
  type        = bool
  default     = true
  description = "Force destroy the S3 bucket"
}

variable "eb_s3_bucket_name" {
  type = string
}

variable "selected_ec2_subnets" {
  type = list
}

variable "selected_elb_subnets" {
  type = list
}


variable "elb_healthcheck_path" {
  type    = string
}