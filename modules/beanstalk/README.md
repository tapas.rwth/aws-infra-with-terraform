# --------Create Elastic Beanstalk ----------
Create eb application
Create eb environemnt within the eb application



#### How to use the module
````
module "my_elasticbeanstalk_app" {
  source               = "<module path relative to the code execution path>"
  eb_app_name          = local.my_eb_app_name
  eb_env_name          = local.my_eb_env_name
  solution_stack_name  = var.solution_stack_name
  application_tier     = var.application_tier
  aws_vpc_id           = module.my_vpc.vpc_id
  selected_ec2_subnets = module.my_vpc.public_subnets_ids
  selected_elb_subnets = module.my_vpc.public_subnets_ids
  asg_min_size         = var.asg_min_size
  asg_max_size         = var.asg_max_size
  region_name          = var.region_name
  vpc_name             = local.my_vpc_name
  vpc_cidr             = var.vpc_cidr
  ec2_keypair          = var.ec2_keypair
  igw_name             = local.my_igw_name
  instance_type        = var.instance_type
  bucket_force_destroy = var.bucket_force_destroy
  elb_healthcheck_path = var.elb_healthcheck_path
  eb_s3_bucket_name    = local.my_eb_s3_bucket_name

}

example arguments:
eb_app_name          = "My-eb-docker-app"
eb_env_name          = "My-eb-docker-app-env"
solution_stack_name  = "64bit Amazon Linux 2023 v4.1.2 running Docker"
application_tier     = "WebServer"
asg_min_size         = "2"
asg_max_size         = "4"
ec2_keypair          = "tapasjana-keypair"
instance_type        = "t2.micro"
bucket_force_destroy = true
elb_healthcheck_path = "/login"
aws_vpc_id           = <created by vpc module>
selected_ec2_subnets = <created by vpc module>
selected_elb_subnets = <created by vpc module>
````

---To run the terraform code and create infra----
````
available env: [dev, stage, prod]

create namespaces
$terraform workspace new dev
$terraform workspace new stage

select namespaces and run code
$terraform workspace select <namespace_name>
$terraform plan
$terrafrom apply/destroy

````
# test auto-scaling by giving load with apache-bench tool
$ab -n 10000 -c 1000 <url>


