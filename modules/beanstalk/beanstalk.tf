resource "aws_s3_bucket" "eb_s3" {
    bucket = "${var.eb_s3_bucket_name}"
    force_destroy = var.bucket_force_destroy
    tags = {
      EB_APP_NAME = var.eb_app_name
  }
}

/*
resource "aws_s3_object" "eb_s3_object" {
    bucket = "${aws_s3_bucket.eb_s3.id}"
    key    = "app/app.zip"
    source = "app.zip"
    force_destroy = var.force_destroy
}
*/

# --------- Create Elastic Beanstalk App-----------------
resource "aws_elastic_beanstalk_application" "eb_app" {
  name        = "${var.eb_app_name}"
  description = "Elastic Beanstalk Application"
  tags = {
      EB_APP_NAME = var.eb_app_name
  }
}

/*
resource "aws_elastic_beanstalk_application_version" "eb_bucket_version" {
  name        =  "${var.eb_app_name}-version-${lookup(var.env_name, terraform.workspace)}"
  application = aws_elastic_beanstalk_application.eb_app.name
  description = "application version created by terraform"
  bucket      = "${aws_s3_bucket.eb_s3.id}"
  key         = "${aws_s3_object.eb_s3_object.id}"
}

*/

# --------- Create Elastic Beanstalk App Environment-----------------
resource "aws_elastic_beanstalk_environment" "eb_env" {
  name        = "${var.eb_env_name}"
  application = aws_elastic_beanstalk_application.eb_app.name
  solution_stack_name = var.solution_stack_name
  tier  = var.application_tier
  tags = {
      EB_APP_NAME = var.eb_app_name
  }
# --------- Service Access Settings -----------------
setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "IamInstanceProfile"
    value     =  "aws-elasticbeanstalk-ec2-role"
  }
setting {
    namespace = "aws:elasticbeanstalk:environment"
    name      = "ServiceRole"
    value     = "aws-elasticbeanstalk-service-role"
    }
setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "EC2KeyName"
    value     = var.ec2_keypair
    }

# -----------VPC Settings------------------
setting {
    namespace = "aws:ec2:vpc"
    name      = "VPCId"
    value     =  var.aws_vpc_id # Replace with your VPC ID
    }
setting {
    namespace = "aws:ec2:vpc"
    name      = "AssociatePublicIpAddress"
    value     =  "True"
  }
setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "DisableIMDSv1"
    value     = "true"
    }  
setting {
    namespace = "aws:ec2:vpc"
    name      = "Subnets"
    value     = join(",",toset(var.selected_ec2_subnets))
    #value =    local.selected_public_subnets
    }

setting {
    namespace = "aws:ec2:vpc"
    name      = "ELBSubnets"
    value     = join(",",toset(var.selected_elb_subnets))
    #value =    local.selected_public_subnets
  }
# -----------Auto Scaling --------------
setting {
    namespace = "aws:ec2:vpc"
    name      = "ELBScheme"
    value     = "internet facing"
    }
setting {
    namespace = "aws:autoscaling:asg"
    name      = "MinSize"
    value     = "${var.asg_min_size}"
    }
setting {
    namespace = "aws:autoscaling:asg"
    name      = "MaxSize"
    value     = "${var.asg_max_size}"
    }
setting {
    namespace = "aws:elasticbeanstalk:environment"
    name      = "EnvironmentType"
    value     = "LoadBalanced"
  }
setting {
    namespace = "aws:elasticbeanstalk:environment"
    name      = "LoadBalancerType"
    value     = "application"
}

setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "InstanceType"
    value     = "${var.instance_type}"
  }

setting {
    namespace = "aws:elasticbeanstalk:cloudwatch:logs"
    name      = "StreamLogs"
    value     = true
    }

setting {
    namespace = "aws:elasticbeanstalk:cloudwatch:logs"
    name      = "DeleteOnTerminate"
    value     = true
    }
setting {
    namespace = "aws:elasticbeanstalk:cloudwatch:logs"
    name      = "RetentionInDays"
    value     = 7
    }

setting {
    namespace = "aws:elasticbeanstalk:healthreporting:system"
    name      = "SystemType"
    value     = "enhanced"
    }
  setting {
    namespace = "aws:autoscaling:updatepolicy:rollingupdate"
    name      = "RollingUpdateEnabled"
    value     = true
    }

# ----------- Cloud Watch logs ----------------------------

setting {
    namespace = "aws:elasticbeanstalk:hostmanager"
    name      = "LogPublicationControl"
    value     = false
    }

setting {
    namespace = "aws:elasticbeanstalk:cloudwatch:logs"
    name      = "StreamLogs"
    value     = true
    }

  setting {
    namespace = "aws:elasticbeanstalk:cloudwatch:logs"
    name      = "DeleteOnTerminate"
    value     = true
  }

  setting {
    namespace = "aws:elasticbeanstalk:cloudwatch:logs"
    name      = "RetentionInDays"
    value     = 7
    }


 ###=========================== Load Balancer ========================== ###

setting { 
    namespace = "aws:elbv2:listener:default"
    name      = "ListenerEnabled"
    value     = false
    }
/*
  setting {
      namespace = "aws:elbv2:listener:443"
      name      = "ListenerEnabled"
      value     = true
  }
  setting {
      namespace = "aws:elbv2:listener:443"
      name      = "Protocol"
      value     = "HTTPS"
   }
  setting {
      namespace = "aws:elbv2:listener:443"
      name      = "SSLCertificateArns"
      value     = var.certificate
  }
  setting {
      namespace = "aws:elbv2:listener:443"
      name      = "SSLPolicy"
      value     = "ELBSecurityPolicy-2016-08"
    
  }
  */

setting {
      namespace = "aws:elasticbeanstalk:environment:process:default"
      name      = "HealthCheckPath"
      value     = var.elb_healthcheck_path
  }
  setting {
      namespace = "aws:elasticbeanstalk:environment:process:default"
      name      = "Port"
      value     = 80
  }
  setting {
      namespace = "aws:elasticbeanstalk:environment:process:default"
      name      = "Protocol"
      value     = "HTTP"
  }


###=========================== Autoscale trigger ========================== ###

  setting {
    namespace = "aws:autoscaling:trigger"
    name      = "MeasureName"
    value     = "CPUUtilization"
    
  }

  setting {
    namespace = "aws:autoscaling:trigger"
    name      = "Statistic"
    value     = "Average"
    
  }

  setting {
    namespace = "aws:autoscaling:trigger"
    name      = "Unit"
    value     = "Percent"
    
  }

  setting {
    namespace = "aws:autoscaling:trigger"
    name      = "LowerThreshold"
    value     = 30
    
  }

  setting {
    namespace = "aws:autoscaling:trigger"
    name      = "LowerBreachScaleIncrement"
    value     = -1
    
  }

  setting {
    namespace = "aws:autoscaling:trigger"
    name      = "UpperThreshold"
    value     = 80
    
  }

  setting {
    namespace = "aws:autoscaling:trigger"
    name      = "UpperBreachScaleIncrement"
    value     = 1
    
  }

  setting {
    namespace = "aws:elasticbeanstalk:managedactions"
    name      = "ManagedActionsEnabled"
    value     = "true"
  }

  setting {
    namespace = "aws:elasticbeanstalk:managedactions"
    name      = "PreferredStartTime"
    value     = "Tue:10:00"
  }

  setting {
    namespace = "aws:elasticbeanstalk:managedactions:platformupdate"
    name      = "UpdateLevel"
    value     = "minor"
  }

  setting {
    namespace = "aws:elasticbeanstalk:managedactions:platformupdate"
    name      = "InstanceRefreshEnabled"
    value     = "true"
  }

}

# --------------- ELB AND SG ------------

resource "aws_lb_listener" "https_redirect" {
   load_balancer_arn = aws_elastic_beanstalk_environment.eb_env.load_balancers[0]
   port              = 80
   protocol          = "HTTP"
   default_action {
      type  = "forward"
      target_group_arn = data.aws_lb_target_group.elb_tg.id
    }
}

data "aws_lb_target_group" "elb_tg" {
  tags = {
    Name = "${aws_elastic_beanstalk_environment.eb_env.name}"
  }
}

data "aws_lb" "eb_lb" {
  arn = aws_elastic_beanstalk_environment.eb_env.load_balancers[0]
}

resource "aws_security_group_rule" "allow_80" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = tolist(data.aws_lb.eb_lb.security_groups)[0]
}
